const players = [
	{
		id: 1,
		name: "lionel messi",
		category: "football",
		img: "./images/lm10.jpg",
	},
	{
		id: 2,
		name: "Critiano Ronaldo",
		category: "football",
		img: "./images/cr7.jpg",
	},
	{
		id: 3,
		name: "connor mcGreger",
		category: "ufc",
		img: "./images/connor.jpeg",
	},
	{
		id: 4,
		name: "Rojer federer",
		category: "tennis",
		img: "./images/federe.jpeg",
	},
	{
		id: 5,
		name: "khabib nurmegomedov",
		category: "ufc",
		img: "./images/jhabib.jpeg",
	},
	{
		id: 6,
		name: "kobe bryant",
		category: "basket-ball",
		img: "./images/kobe.jpeg",
	},
	{
		id: 7,
		name: "lionel messi",
		category: "football",
		img: "./images/lm10.jpg",
	},
	{
		id: 8,
		name: "lebron",
		category: "basket-ball",
		img: "./images/lebron.jpeg",
	},
	{
		id: 9,
		name: "lewis hamilton",
		category: "formula1",
		img: "./images/lewis.jpeg",
	},
	{
		id: 10,
		name: "maradona",
		category: "football",
		img: "./images/maradona.jpg",
	},
	{
		id: 11,
		name: "neymer jr",
		category: "motogp",
		img: "./images/marq.jpeg",
	},
	{
		id: 12,
		name: "max versthpan",
		category: "formula1",
		img: "./images/max.jpeg",
	},
	{
		id: 13,
		name: "nadal",
		category: "tennis",
		img: "./images/nadal.jpeg",
	},
	{
		id: 14,
		name: "pele",
		category: "football",
		img: "./images/pele.jpg",
	},
	{
		id: 15,
		name: "valentini rossi",
		category: "motogp",
		img: "./images/vr.jpg",
	},
];

const playerContent = document.querySelector(".content");
const filterBtns = document.querySelector(".btn-container");

window.addEventListener("DOMContentLoaded", function () {
	displayPlayerItems(players);
	filterPlayers();
});

function displayPlayerItems(playerItems) {
	let displayPlayers = playerItems
		.map(function (item) {
			return `<article>
                    <a href="#"
                        ><img src=${item.img} alt="Image"
                    /></a>
                    <span>${item.category}</span>
                </article>`;
		})
		.join("");
	playerContent.innerHTML = displayPlayers;
}

function filterPlayers() {
	const categories = players.reduce(
		function (values, item) {
			if (!values.includes(item.category)) {
				values.push(item.category);
			}
			return values;
		},
		["all"]
	);
	const categoryBtn = categories
		.map(function (categoryItem) {
			return `<button type="button" data-id=${categoryItem}>${categoryItem}</button>`;
		})
		.join("");

	filterBtns.innerHTML = categoryBtn;
	const btnForMethod = filterBtns.querySelectorAll("button");
	console.log(btnForMethod);

	btnForMethod.forEach((btn) => {
        btn.addEventListener('click',(e)=>{
            const btnCategory = e.currentTarget.dataset.id;
            console.log(btnCategory);
            const playerCategory = players.filter((playerItem)=>{
                if(playerItem.category === btnCategory){
                    return playerItem;
                }
            }) 
            if(btnCategory === 'all'){
                displayPlayerItems(players);
            }else{
                displayPlayerItems(playerCategory)
            }
        })
    });
}
